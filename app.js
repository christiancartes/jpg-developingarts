(function() {
    const SCROLL_DURATION = 300;
    const CIRCLE_ANIMATION = 0.65;
    // left: 37, up: 38, right: 39, down: 40
    const KEYBOARD_KEYS = { 37: 1, 38: 1, 39: 1, 40: 1 };

    var m_sectionDataJson;
    var m_sectionSelectedName;
    var m_sectionSelectedNumber;
    var m_couldRunTweens = true;
    //This variable stores the last selected section when there is an animation in progress between two sections
    var m_lastSelectedSectionName;
    var m_transitionIsRunning = false;
    var m_isScrollPageLaunchedByPager = false;


    $(document).ready(function() {
        $(window).scrollTop(0);
        if (ResponsiveBootstrapToolkit.is("<=sm")) {
            m_couldRunTweens = false;
        }

        //Pager component
        $('#navbar-scroll-spy li a').click(OnPagerClicked);

        //init ScrollMagic
        var controller = new ScrollMagic.Controller();
        //build scene
        $(".section").each(function() {
            var scene = new ScrollMagic.Scene({
                    triggerElement: this,
                    triggerHook: 0.35
                })
                .setClassToggle(this.children[0], 'fade-in')
                .addIndicators({
                    name: "SCENE-TRIGGER",
                    colorTrigger: "black",
                    indent: 200,
                    colorStart: '#FF0505',
                    colorEnd: '#FF0000'
                })
                .addTo(controller);
            // add listeners
            scene.on("enter leave", OnEnterAndLeaveCallback);
        });

        //Just in case of a resize event
        $(window).resize(
            ResponsiveBootstrapToolkit.changed(function() {
                if (ResponsiveBootstrapToolkit.is("<=sm")) {
                    m_couldRunTweens = false;
                } else {
                    m_couldRunTweens = true;
                }
            })
        );

        //Calls the section configuration that contains the configuration for each section
        var xhr = new XMLHttpRequest();
        xhr.open('GET', "data/data.json", true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(null);
        xhr.onreadystatechange = OnxhrRequestCompleted;

        //Listeners for category work buttons
        $(".category_item").on("click", function() {
            $('#right-container').removeClass().toggleClass('col-md-8');
            $('#right-container').on('transitionend',
                function() {

                    //grid test
                    createCategories();

                    //Hide the 
                    $('.section_list').css("opacity", "0");
                    //Shows
                    $('.category_selected').css("display", "inline-block");

                    $('#right-container').off('transitionend');
                    $('#middle-container').toggleClass('col-md-4 col-md-8');
                    $('#right-container').removeClass().toggleClass('col-0');
                });
        });
    });

    function createCategories() {
        var colour = [
            "rgb(142, 68, 173)",
            "rgb(243, 156, 18)",
            "rgb(211, 84, 0)",
            "rgb(0, 106, 63)",
            "rgb(41, 128, 185)",
            "rgb(192, 57, 43)",
            "rgb(135, 0, 0)",
            "rgb(39, 174, 96)"
        ];

        $(".brick").each(function() {
            this.style.backgroundColor = colour[colour.length * Math.random() << 0];
        });

        var wall = new Freewall("#freewall");
        wall.reset({
            selector: '.brick',
            animate: true,
            cellW: 160,
            cellH: 160,
            delay: 50,
            onResize: function() {
                wall.fitWidth();
            }
        });
        wall.fitWidth();

        var temp = '<div class="brick {size}" style="background-color: {color}"><div class="cover"></div></div>';
        var size = "size33 size32 size31 size23 size22 size21 size13 size12 size11".split(" ");
        $(".add-more").click(function() {
            var html = "";
            for (var i = 0; i < 1; ++i) {
                html += temp.replace('{size}', size[size.length * Math.random() << 0])
                    .replace('{color}', colour[colour.length * Math.random() << 0]);
            }
            wall.appendBlock(html);
        });
    }

    function OnxhrRequestCompleted(xhrResponse) {
        if (this.readyState == 4) {
            m_sectionDataJson = JSON.parse(this.responseText);
        }
    }

    function OnEnterAndLeaveCallback(event) {
        console.log("------- OnEnterAndLeaveCallback");
        var sectionNumber, sectionHtmlEnter, sectionSelectedName, isLeaveEvent;

        sectionHtmlEnter = event.currentTarget.triggerElement();
        sectionSelectedName = sectionHtmlEnter.id;

        isLeaveEvent = event.type == "leave";

        if (isLeaveEvent) {
            sectionNumber = sectionSelectedName.match(/\d+/)[0];
            sectionSelectedName = "section" + (--sectionNumber);
            m_sectionSelectedNumber = sectionNumber;
        }
        m_sectionSelectedName = sectionSelectedName;

        changeSection(m_sectionSelectedName, isLeaveEvent);
    }

    function changeSection(sectionSelectedName, isLeaveEvent) {
        var promise;

        disableScroll();

        $('#content-section').removeClass().addClass("container-fluid main-container " + sectionSelectedName);

        switch (sectionSelectedName) {
            case "section0":
                if (isLeaveEvent) {
                    promise = inOutLogoAnTitle(sectionSelectedName, isLeaveEvent, true);

                    promise.then(function() {
                        logoAnimationIn();
                    });
                }
                break;
            case "section1":
                if (!isLeaveEvent) {
                    promise = logoAnimationLeftSide();
                    promise.then(function() {
                        var promiseLogoTitle = inOutLogoAnTitle(sectionSelectedName, isLeaveEvent, false);
                        promiseLogoTitle.then(function() {
                            enableScroll();
                        });
                    });
                } else {
                    initCircleAnimation();
                    promise = inOutLogoAnTitle(sectionSelectedName, isLeaveEvent, false);
                    promise.then(function() {
                        enableScroll();
                    });
                }
                break;
            default:
                promise = inOutLogoAnTitle(sectionSelectedName, isLeaveEvent, false);
                promise.then(function() {
                    enableScroll();
                });
                initCircleAnimation();
                break;
        }
    }

    function inOutLogoAnTitle(sectionSelectedName, isLeaveEvent, isFirstSection) {
        return new Promise(function(result, request) {
            var removeListener, isInOutAnimation;

            isInOutAnimation = 0;
            removeListener = true;

            $('.section_logo_title .section_moving_div').removeClass().addClass('section_moving_div');

            if (isFirstSection) {
                $('.section_logo_title .section_moving_div').addClass('right_animation_container_out');
            } else {
                if (sectionSelectedName === "section1") {
                    if (isLeaveEvent) {
                        isInOutAnimation = 1;
                        $('.section_logo_title .section_moving_div').addClass('right_animation_container_out');
                    } else {
                        changeDataOfTitleAndLogo(sectionSelectedName);
                        $('.section_logo_title .section_moving_div').addClass('right_animation_container_in');
                    }
                } else {
                    isInOutAnimation = 1;
                    $('.section_logo_title .section_moving_div').addClass('right_animation_container_out');
                }
            }
            $('.section_logo_title .section_moving_div').on('webkitAnimationEnd oanimationend msAnimationEnd animationend',
                function() {
                    if (isInOutAnimation > 0) {
                        if (isInOutAnimation == 1) {
                            isInOutAnimation = 2;
                            removeListener = false;
                            changeDataOfTitleAndLogo(sectionSelectedName);
                            $('.section_logo_title .section_moving_div').removeClass('right_animation_container_out');
                            $('.section_logo_title .section_moving_div').addClass('right_animation_container_in');

                        } else if (isInOutAnimation == 2) {
                            isInOutAnimation = 0;
                            removeListener = true;
                        }
                    }

                    if (removeListener) {
                        $('.section_logo_title .section_moving_div').removeClass('right_animation_container_out');
                        $('.section_logo_title .section_moving_div').off('webkitAnimationEnd oanimationend msAnimationEnd animationend');
                        result();
                    }
                });
        });
    }

    function changeDataOfTitleAndLogo(sectionSelectedName) {
        var sectionSelectedData = getDataOfSection(sectionSelectedName);

        if (sectionSelectedData) {
            $('.section_logo_title .section_title').text(sectionSelectedData.title)
            $('.section_logo_title .section_image').attr("src", sectionSelectedData.img);
        }
    }

    /**
     * Animates the logo to the center of the screen
     */
    function logoAnimationIn() {
        setTimeout(function() {
            $('#middle-container').toggleClass('col-md-4 col-0');
            $('#right-container').toggleClass('col-md-4 col-md-8');
        }, 500);

        var timeLineAnimator = new TimelineMax({ onComplete: onLogoAnimationInFinished });

        timeLineAnimator.to("#company_logo_id .logo_title", 0.10, {
            delay: 0.6,
            scale: 0.9
        });
        timeLineAnimator.to("#company_logo_id .logo_title", 0.10, {
            scale: 1.1
        });
        timeLineAnimator.to("#company_logo_id .logo_title", 0.10, {
            opacity: 0,
            scale: 1
        });
        timeLineAnimator.to("#company_logo_id", 0.55, {
            bezier: [
                { left: '28%', top: '70%', scale: 0.4 },
                { left: '50%', top: '50%', scale: 1 }
            ]
        });
    }

    /**
     * Animates the logo to the left side of the screen
     */
    function logoAnimationLeftSide() {
        return new Promise(function(result, request) {
            $('#middle-container').toggleClass('col-0 col-md-4');
            $('#right-container').toggleClass('col-md-8 col-md-4');

            var timeLineAnimator = new TimelineMax({
                onComplete: function() {
                    result();
                }
            });

            timeLineAnimator.to("#company_logo_id", 0.55, {
                bezier: [
                    { left: '28%', top: '70%', scale: 0.4 },
                    { left: '17%', top: '28%', scale: 0.5 }
                ]
            });

            timeLineAnimator.to("#company_logo_id .logo_title", 0.15, {
                opacity: 1,
                scale: 1.1
            });
            timeLineAnimator.to("#company_logo_id .logo_title", 0.15, {
                scale: 0.9
            });
            timeLineAnimator.to("#company_logo_id .logo_title", 0.15, {
                scale: 1
            });
        });
    }

    function initCircleAnimation() {
        m_transitionIsRunning = true;
        if (m_couldRunTweens) {
            $("#bg_color_left .circle").each(function(index) {
                $(this).css({
                    top: 100 * (index / 6) + "%",
                    left: getRandomRange(15, 85) + "%",
                    width: 0
                });
            });

            $("#bg_color_right .circle").each(function(index) {
                $(this).css({
                    top: 100 * (index / 6) + "%",
                    left: getRandomRange(15, 85) + "%",
                    width: 0
                });

            });

            TweenMax.to("#bg_color_left .circle", CIRCLE_ANIMATION, {
                width: "200%",
                ease: Elastic.Linear
            });
            TweenMax.to("#bg_color_right .circle", CIRCLE_ANIMATION, {
                width: "200%",
                ease: Elastic.Linear,
                onComplete: OnCompleteTween
            });
        } else {
            $("#bg_color .circle").css({ transition: 'width 0.4s' })
            $("#bg_color .circle").addClass("animated_circle");

            setTimeout(function() {
                $("#bg_color").removeClass().addClass(m_sectionSelectedName);
                $("#bg_color .circle").removeClass("animated_circle");
                m_transitionIsRunning = false;
            }, 500);
        }
    }

    function onLogoAnimationInFinished() {
        enableScroll();
    }

    function OnCompleteTween() {
        m_transitionIsRunning = false;
        $("#bg_color_left").removeClass().addClass(m_sectionSelectedName);
        $("#bg_color_right").removeClass().addClass(m_sectionSelectedName);
    }

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    function preventDefaultForScrollKeys(e) {
        if (KEYBOARD_KEYS[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function disableScroll() {
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove = preventDefault; // mobile
        document.onkeydown = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }

    function OnPagerClicked(event) {
        // Prevent the jump and the #hash from appearing on the address bar
        event.preventDefault();
        if (m_couldRunTweens && !m_isScrollPageLaunchedByPager) {

            m_isScrollPageLaunchedByPager = true;

            // Scroll the window, stop any previous animation, stop on user manual scroll
            $(window).stop(true).scrollTo(this.hash, { duration: SCROLL_DURATION, interrupt: true, complete: OnScrollCompleted });

            var hash = event.currentTarget.hash;
            sectionSelectedName = hash.replace("#", "");

            $('#content-section').removeClass().addClass("container-fluid main-container " + sectionSelectedName);
            initCircleAnimation();
        }
    }

    function OnScrollCompleted() {
        m_isScrollPageLaunchedByPager = false;
    }

    function getRandomRange(min, max) {
        return Math.random() * (max - min) + min;
    }

    function getDataOfSection(sectionName) {
        var i, dataJson, auxData;
        for (i = 0; i < m_sectionDataJson.length; i++) {
            auxData = m_sectionDataJson[i].sectionId;
            if (auxData === sectionName) {
                dataJson = m_sectionDataJson[i];
                break;
            }
        }
        return dataJson;
    }
})();